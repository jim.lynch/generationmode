################################################################
#
#  Script will use curl to set the generationMode on the SBC.
#
################################################################

ARGS=4
if [ $# -ne "$ARGS" ]
then
  echo Usage: $0 user password sbcsfile mode-setting
  echo "      where user and password are the login credentials for the SBCs"
  echo "            sbcsfile is file that contains the SBC IPs, one per line"
  echo "            mode-setting must be either 'origination', 'destination', 'allcalls' or 'none'"
  exit 0
fi

USER=$1
PASSWORD=$2
SBCFILE=$3
MODE=$4

##  Validate the mode

modes=('origination' 'destination' 'allcalls' 'none')
if ! [[ "${modes[@]}" =~ $MODE ]]; then
    echo MODE $MODE does not exist
    exit
fi

CURL_RESPONSE_FILE=CurlResponse_$$.txt
SET_GENERATIONMODE=api/config/oam/accounting/admin/generationMode
GET_SYSTEM_NAME=api/config/system/admin

##  Read the list of SBC IPs
SBCS=( $(< $SBCFILE) )

##  For each SBC in the list, set the generationMode
for sbcip in ${SBCS[@]}
do
    ##  Ignore lines that start with #
    if [[ $sbcip == "#"* ]]; then
        continue
    fi 

    ##  Make sure the SBC is reachable

    ping -c 1 -W 2 $sbcip > /dev/null
    if [ $? != 0 ]; then
        echo SBC $sbcip is not reachable
        continue
    fi

    ##  Make the REST request to set the generationMode.
    ##  Response written to the CURL_RESPONSE_FILE

    http_status=$(curl -XPUT -o $CURL_RESPONSE_FILE -w "%{http_code}" -ksu ${USER}:${PASSWORD} -H 'Content-Type: application/vnd.yang.data+xml' https://${sbcip}/${SET_GENERATIONMODE} --data '<generationMode>'"$MODE"'</generationMode>')
    if [ $http_status == "204" ]; then

        ##  Get the system name just for informational purposes
        http_status=$(curl -o $CURL_RESPONSE_FILE -w "%{http_code}" -ksu admin:hoI!d6DGlp*8VQi~WSw3Ej^YTb https://${sbcip}/${GET_SYSTEM_NAME})
        if [ $http_status == "200" ]; then
            systemName=`cat $CURL_RESPONSE_FILE | grep "<name>" | cut -d ">" -f2  | cut -d "<" -f1`
            echo SBC $sbcip \($systemName\) successfully set generationMode to  ${MODE}  
        fi
    else
        echo Failed to set generationMode for $sbcip, status is $http_status
    fi
done