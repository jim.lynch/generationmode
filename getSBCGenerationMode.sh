################################################################
#
#  Script will use curl to get SBC setting of generationMode.
#
################################################################

ARGS=3
if [ $# -ne "$ARGS" ]
then
  echo Usage: $0 user password sbcsfile
  echo "      where user and password are the login credentials for the SBCs"
  echo "            sbcsfile is file that contains the SBC IPs, one per line"
  exit 0
fi

USER=$1
PASSWORD=$2
SBCFILE=$3

CURL_RESPONSE_FILE=CurlResponse_$$.txt
GET_GENERATIONMODE=api/config/oam/accounting/admin/generationMode
GET_SYSTEM_NAME=api/config/system/admin

##  Read the list of SBC IPs
SBCS=( $(< $SBCFILE) )

for sbcip in ${SBCS[@]}
do
    ##  Ignore lines that start with #
    if [[ $sbcip == "#"* ]]; then
        continue
    fi 

    ##  Make sure the SBC is reachable

    ping -c 1 -W 3 $sbcip > /dev/null
    if [ $? != 0 ]; then
        echo SBC $sbcip is not reachable
        continue
    fi

    ##  Make the REST request to get the generationMode.
    ##  Response written to the CURL_RESPONSE_FILE

    http_status=$(curl -o $CURL_RESPONSE_FILE -w "%{http_code}" -ksu ${USER}:${PASSWORD} https://${sbcip}/${GET_GENERATIONMODE})
    if [ $http_status == "200" ]; then
        ##  Extract the generationMode setting
        generationMode=`cat $CURL_RESPONSE_FILE | cut -d ">" -f2  | cut -d "<" -f1 | tr -d '\n'`

        ##  Get the system nme just for informational purposes
        
        http_status=$(curl -o $CURL_RESPONSE_FILE -w "%{http_code}" -ksu ${USER}:${PASSWORD} https://${sbcip}/${GET_SYSTEM_NAME})
        if [ $http_status == "200" ]; then
            systemName=`cat $CURL_RESPONSE_FILE | grep "<name>" | cut -d ">" -f2  | cut -d "<" -f1`
            echo SBC $sbcip \($systemName\) generationMode = "'${generationMode}'"  
        fi
    else
        echo Failed to get generationMode from $sbcip, status is $http_status
    fi
done
