#!/usr/bin/env python3

###################################################################################
#
#   setSBCGenerationMode.py
#
#   Use SBC REST API to set the OAM Accounting generationMode for a list of SBCs.
#
###################################################################################

import argparse
import requests
import json
import urllib3
import base64
import syslog
import os
import sys
from pathlib import Path
import subprocess
import xmltodict
from requests.auth import HTTPBasicAuth
import pprint
import pdb 

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

pgmName = Path(sys.argv[0]).stem


########################################################
#
#  Method: main
#
########################################################
def main():

    sbcEps = []

    pgmName = Path(sys.argv[0]).stem

    #  Setup to parse the command line arguments

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    required= parser.add_argument_group(title='Required arguments')
    optional= parser.add_argument_group(title='Optional arguments')

    optional.add_argument('-d',  action='store_true', default=False, dest='bDebug', help='Enable debug logging (optional)')

    required.add_argument('-f',  action='store', dest='sbcsFile',  
                          help=
                          "Contains the SBC IPs to be queried, one IP per line.   Lines that start with a # are treated as comments."
                          )
    required.add_argument('-u',  action='store', dest='sbcuser', required=True, help='SBC (CLI) user ID')
    required.add_argument('-p',  action='store', dest='sbcpassword', required=True, help='SBC user password')
    required.add_argument('-m',  action='store', dest='mode', required=True, choices=['origination','destination', 'allcalls',  'none'], help='Generation mode (origination, destination, allcalls or none)')

    #  Print help if no arguments

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    #  Parse the arguments

    args = parser.parse_args()    #  Get the arguments

    if args.bDebug == True:
        pdb.set_trace()

    ##  Read the list of SBC IPs to query

    sbcsList = []
    try:
        with open(args.sbcsFile) as file:
            sbcsList = [line.rstrip() for line in file]
    except Exception as e:
        print("Failed to open file {}, error={}".format(args.sbcsFile, e))
        sys.exit(1)

    ##  For each SBC in the list....
    ##      1. Ping the IP. If no response, skip it
    ##      2. Get the server name and set the generationMode

    for sbcIP in sbcsList:
        ##  If empty line, obviously skip it
        if len(sbcIP) == 0:
            continue

        ##  Comments start with a #
        if sbcIP[0] == '#':
            continue

        pingResp = os.system("ping -c 1 -W 2 " + sbcIP + " >/dev/null")
        if pingResp != 0:
            print("SBC {} is not reachable".format(sbcIP))
            continue

        ##  Ping successful, get the system name

        curlCommand = ['curl', '-sku', "{}:{}".format(args.sbcuser, args.sbcpassword), "https://{}/api/config/system/admin".format(sbcIP)]
        response = subprocess.check_output(curlCommand)

        ##  Decode the byte string response, convert from XML to a dictionary and get the system name
        systemNameDict = xmltodict.parse(response.decode("utf-8"))
        serverName = systemNameDict['collection']['admin']['name']

        ##  Now set the generationMode 

        auth="{}:{}".format(args.sbcuser, args.sbcpassword)
        url = "https://{}/api/config/oam/accounting/admin/".format(sbcIP)
        data = '<admin><generationMode>{}</generationMode></admin>'.format(args.mode)
        curlCmd= "curl -w %{http_code} " +  " -XPUT {} -sku {}  -H 'Content-Type: application/vnd.yang.data+xml' -d '{}'".format(url, auth, data)

        proc = subprocess.Popen(curlCmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (response, error) = proc.communicate()

        ##  Process the set 'generationMode' response
        status = response.decode("utf-8")

        ##  Http response of 204 indicates successful PUT
        print("SBC {} ({}) {} set generationMode to {}".format(sbcIP, serverName, "Successfully" if status == "204" else "Failed to ", args.mode))
    
    sys.exit(0)

if __name__ == '__main__':
    main()

